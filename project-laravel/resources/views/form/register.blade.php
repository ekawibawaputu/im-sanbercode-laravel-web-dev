@extends('layout.app')
@section('title')
Buat Account Baru!
@endsection

@section('sub-title')
Sign Up Form
@endsection

@section('content')
<form action="/welcome" method="POST">
    @csrf
        <label>First Name :</label><br><br>
        <input type="text" name="NamaDepan"><br><br>
        <label>Last Name :</label><br><br>
        <input type="text" name="NamaBelakang"><br><br>
        <label>Gender :</label><br><br>
        <input type="radio">Male<br>
        <input type="radio">Female<br>
        <input type="radio">Other<br><br>
        <label>Nationality :</label><br><br>
        <select name="Kebangsaan">
            <option value="Indonesia">Indonesia</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Thailand">Thailand</option>
        </select><br><br>
        <label>Language Spoken :</label><br><br>
        <input type="checkbox" name="bahasa">Bahasa Indonesia<br>
        <input type="checkbox" name="bahasa">English<br>
        <input type="checkbox" name="bahasa">Other<br><br>
        <label>Bio :</label><br><br>
        <textarea name="message" rows="10" cols="30"></textarea><br>
        <input type="submit" value="Sign Up">
@endsection
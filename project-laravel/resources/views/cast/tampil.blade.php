@extends('layout.app')

@section('title')
Halaman Tampil Cast
@endsection

@section('sub-title')
Cast
@endsection

@section('content')

<a href="/cast/create" class="btn btn-primary btn-sm my-3">Tambah</a>

<table class="table">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">nama</th>
            <th scope="col">action</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->nama}}</td>
                <td>
                    <form action="/cast/{{$item->id}} method="POST">
                        @method('delete')
                        @csrf
                        <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            
        @endforelse
    </tbody>
</table>

@endsection
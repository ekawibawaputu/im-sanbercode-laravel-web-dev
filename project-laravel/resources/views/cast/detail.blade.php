@extends('layout.app')

@section('title')
Halaman Detail Cast
@endsection

@section('sub-title')
Cast
@endsection

@section('content')
<h1>{{$role->nama}}</h1>
<p>{{$role->umur}}</p>
<p>{{$role->bio}}</p>

<a href="/cast" class="btn btn-secondary btn-sm">Kembali</a>

@endsection
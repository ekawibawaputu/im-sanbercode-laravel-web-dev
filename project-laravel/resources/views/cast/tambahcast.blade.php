@extends('layout.app')

@section('title')
Halaman Tambah Cast
@endsection

@section('sub-title')
Cast
@endsection

@section('content')
<form action="/cast" method="POST">
    @csrf
        <div class="form-group">
            <label>Cast Nama</label>
            <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Cast Umur</label>
            <input type="text" class="form-control @error('umur') is-invalid @enderror" name="umur">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Cast Bio</label>
            <textarea name="bio" class="form-control @error('bio') is-invalid @enderror"></textarea>
        </div>

        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*

|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'halaman']);
Route::get('/register', [AuthController::class, 'regis']);

Route::post('/welcome', [AuthController::class, 'send']);

Route::get('/table',function(){
    return view('form.table');
});

Route::get('/data-table', function(){
    return view('form.datatable');
});

// membuat CRUD

Route::get('/cast/create',[CastController::class,'create']);

// route menyimpan data kedalam DB

Route::post('/cast',[CastController::class,'store']);

// route untuk menampilkan semua data pada table cast
Route::get('/cast',[CastController::class, 'index']);
// route data berdasarkan id
Route::get('/cast/{cast_id}', [CastController::class,'show']);

// route untuk mengarah ke form edit kategori
Route::get('/cast/{cast_id}/edit',[CastController::class,'edit']);

// route untuk update data berdasarkan id
Route::put('/cast/{cast_id}',[CastController::class,'update']);

// route untuk melakukan delete data berdasarkan id
Route::delete('/cast/{cast_id}',[CastController::class,'destroy']);
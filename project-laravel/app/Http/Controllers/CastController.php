<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create(){
        return view('cast.tambahcast');
    }

    public function store(Request $request){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ],
        [
            'nama.required' => 'Nama harus diisi',
            'umur.required' => 'Masukan umur',
            'bio.required' => 'Biodata harus diisi'
        ]);

        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);
        return redirect('/cast');  
    }

    public function index(){
        $cast = DB::table('cast')->get();

        // dd($cast);

        return view('cast.tampil', ['cast' => $cast]);
    }

    public function show($id){
        $role = DB::table('cast')->find($id);

        return view('cast.detail', ['role'=>$role]);
    }

    public function edit($id){
        $role = DB::table('cast')->find($id);

        return view('cast.edit', ['role'=>$role]);
    }

    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ],
        [
            'nama.required' => 'Nama harus diisi',
            'umur.required' => 'Masukan umur',
            'bio.required' => 'Biodata harus diisi'
        ]);

        DB::table('cast')
            ->where('id', $id)
            ->update([
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio']
            ]);
        return redirect('/cast');
    }

    public function destroy($id){
        DB::table('cast')->where('id',$id)->delete();
        
        return redirect('/cast');
    }
}
